package example.pertanianjagung;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;

import example.pertanianjagung.assets.Perhitungan;

public class PupukActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText edtLuasLahan, edtPanjang, edtLebar,edtTotalBeratPupuk;
    Button btnHitung, btnReset;
    TextView tvJumlahTanaman,tvPemupukan7,tvPemupukan30;
    MaterialCardView hasilLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pupuk);
        //init toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.pupuk);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init component
        edtLuasLahan = findViewById(R.id.edt_luas_lahan);
        edtPanjang = findViewById(R.id.edt_panjang);
        edtLebar = findViewById(R.id.edt_lebar);
        edtTotalBeratPupuk = findViewById(R.id.edt_berat_total_pupuk);
        tvJumlahTanaman = findViewById(R.id.tv_jumlah_tanaman);
        tvPemupukan7 = findViewById(R.id.tv_pemupukan_7);
        tvPemupukan30 = findViewById(R.id.tv_pemupukan_30);
        hasilLayout = findViewById(R.id.hasil_layout);
        btnHitung = findViewById(R.id.btn_hitung);
        btnReset = findViewById(R.id.btn_reset);
        btnHitung.setOnClickListener(v -> validasiCheck());
        btnReset.setOnClickListener(v -> reset());
    }

    private void reset() {
        edtLuasLahan.setText("");
        edtTotalBeratPupuk.setText("");
        edtLebar.setText("");
        edtPanjang.setText("");
        edtLuasLahan.requestFocus();
        hasilLayout.setVisibility(View.GONE);
    }

    private void validasiCheck() {
        Boolean validasi = true;
        if (edtPanjang.getText().toString().isEmpty()) {
            edtPanjang.setError("Harap diisi");
            edtPanjang.requestFocus();
            validasi = false;
        } else {
            edtPanjang.setError(null);
        }

        if (edtLebar.getText().toString().isEmpty()) {
            edtLebar.setError("Harap diisi");
            edtLebar.requestFocus();
            validasi = false;
        } else {
            edtLebar.setError(null);
        }
        if (edtTotalBeratPupuk.getText().toString().isEmpty()) {
            edtTotalBeratPupuk.setError("Harap diisi");
            edtTotalBeratPupuk.requestFocus();
            validasi = false;
        } else {
            edtTotalBeratPupuk.setError(null);
        }
        if (validasi) {
            Perhitungan.pupuk(
                    Double.parseDouble(edtLuasLahan.getText().toString()),
                    Double.parseDouble(edtPanjang.getText().toString()),
                    Double.parseDouble(edtLebar.getText().toString()),
                    Double.parseDouble(edtTotalBeratPupuk.getText().toString()),
                    tvJumlahTanaman,
                    tvPemupukan7,
                    tvPemupukan30
            );
            hasilLayout.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}