package example.pertanianjagung;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuActivity extends AppCompatActivity {

    @BindView(R.id.menu_populasi)
    TextView menuPopulasi;
    @BindView(R.id.menu_pupuk)
    TextView menuPupuk;
    @BindView(R.id.menu_jarak_tanam)
    TextView menuJarakTanam;
    @BindView(R.id.menu_produksi)
    TextView menuProduksi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        menuPopulasi.setOnClickListener(v -> startActivity(new Intent(MenuActivity.this, PopulasiActivity.class)));
        menuPupuk.setOnClickListener(v -> startActivity(new Intent(MenuActivity.this, PupukActivity.class)));
        menuJarakTanam.setOnClickListener(v -> startActivity(new Intent(MenuActivity.this, JarakTanamActivity.class)));
        menuProduksi.setOnClickListener(v -> startActivity(new Intent(MenuActivity.this, ProduksiActivity.class)));
    }
}