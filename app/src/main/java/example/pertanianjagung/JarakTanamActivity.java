package example.pertanianjagung;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;

import java.math.BigDecimal;

import example.pertanianjagung.assets.Perhitungan;

public class JarakTanamActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText edtLuasLahan, edtJumlahBibit;
    Button btnHitung, btnReset;
    TextView tvJarakAntarTanaman;
    MaterialCardView hasilLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jarak_tanam);
        //init toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Jumlah Tanaman");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init component
        edtLuasLahan = findViewById(R.id.edt_luas_lahan);
        edtJumlahBibit = findViewById(R.id.edt_jumlah_bibit);
        tvJarakAntarTanaman = findViewById(R.id.tv_jarak_antar_tanaman);
        hasilLayout = findViewById(R.id.hasil_layout);
        btnHitung = findViewById(R.id.btn_hitung);
        btnReset = findViewById(R.id.btn_reset);
        btnHitung.setOnClickListener(v -> validasiCheck());
        btnReset.setOnClickListener(v -> reset());
    }

    private void reset() {
        edtLuasLahan.setText("");
        edtJumlahBibit.setText("");
        edtLuasLahan.requestFocus();

        hasilLayout.setVisibility(View.GONE);
    }

    private void validasiCheck() {
        boolean validasi = true;
        if (edtLuasLahan.getText().toString().isEmpty()) {
            edtLuasLahan.setError("Harap diisi");
            edtLuasLahan.requestFocus();
            validasi = false;
        } else {
            edtLuasLahan.setError(null);
        }
        if (edtJumlahBibit.getText().toString().isEmpty()) {
            edtJumlahBibit.setError("Harap diisi");
            edtJumlahBibit.requestFocus();
            validasi = false;
        } else {
            edtJumlahBibit.setError(null);
        }
        if (validasi) hitung();
    }

    @SuppressLint("SetTextI18n")
    private void hitung() {
        Double hasil = Perhitungan.jarak(
                Double.parseDouble(edtLuasLahan.getText().toString()),
                Double.parseDouble(edtJumlahBibit.getText().toString())
        );
        tvJarakAntarTanaman.setText(Html.fromHtml(BigDecimal.valueOf(hasil).toPlainString()+" cm<sup>2</sup>"));

        hasilLayout.setVisibility(View.VISIBLE);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}