package example.pertanianjagung;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.card.MaterialCardView;

import java.math.BigDecimal;

import example.pertanianjagung.assets.Perhitungan;

public class ProduksiActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText edtLuasLahan, edtPanjang, edtLebar;
    Button btnHitung, btnReset;
    TextView tvJumlahPanen;
    MaterialCardView hasilLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produksi);
        //init toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Produksi");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init component
        edtLuasLahan = findViewById(R.id.edt_luas_lahan);
        edtPanjang = findViewById(R.id.edt_panjang);
        edtLebar = findViewById(R.id.edt_lebar);
        tvJumlahPanen = findViewById(R.id.tv_jumlah_panen);
        hasilLayout = findViewById(R.id.hasil_layout);
        btnHitung = findViewById(R.id.btn_hitung);
        btnReset = findViewById(R.id.btn_reset);
        btnHitung.setOnClickListener(v -> validasiCheck());
        btnReset.setOnClickListener(v -> reset());
    }

    private void reset() {
        edtLuasLahan.setText("");
        edtPanjang.setText("");
        edtLebar.setText("");
        edtLuasLahan.requestFocus();
        hasilLayout.setVisibility(View.GONE);
    }

    private void validasiCheck() {
        boolean validasi = true;
        if (edtPanjang.getText().toString().isEmpty()) {
            edtPanjang.setError("Harap diisi");
            edtPanjang.requestFocus();
            validasi = false;
        } else {
            edtPanjang.setError(null);
        }
        if (edtLebar.getText().toString().isEmpty()) {
            edtLebar.setError("Harap diisi");
            edtLebar.requestFocus();
            validasi = false;
        } else {
            edtLebar.setError(null);
        }
        if (edtLuasLahan.getText().toString().isEmpty()) {
            edtLuasLahan.setError("Harap diisi");
            edtLuasLahan.requestFocus();
            validasi = false;
        } else {
            edtLuasLahan.setError(null);
        }
        if (validasi) hitung();
    }

    @SuppressLint("SetTextI18n")
    private void hitung() {

        long hasil = Math.round(Perhitungan.produksi(
                Double.parseDouble(edtLuasLahan.getText().toString()),
                Double.parseDouble(edtPanjang.getText().toString()),
                Double.parseDouble(edtLebar.getText().toString())
        ));
        Log.e("TAG", "hitung: "+hasil );
        tvJumlahPanen.setText(""+ BigDecimal.valueOf(hasil).toPlainString()+" Kg");

        hasilLayout.setVisibility(View.VISIBLE);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}