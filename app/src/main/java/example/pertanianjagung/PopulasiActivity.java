package example.pertanianjagung;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.card.MaterialCardView;

import java.math.BigDecimal;

import example.pertanianjagung.assets.Perhitungan;

public class PopulasiActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText edtLuasLahan, edtP, edtL;
    Button btnHitung, btnReset;
    TextView tvJumlahTanaman;
    MaterialCardView hasilLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_populasi);
        //init toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.populasi);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init component
        edtLuasLahan = findViewById(R.id.edt_luas_lahan);
        edtP = findViewById(R.id.edt_p);
        edtL = findViewById(R.id.edt_l);
        tvJumlahTanaman = findViewById(R.id.tv_jumlah_tanaman);
        hasilLayout = findViewById(R.id.hasil_layout);
        btnHitung = findViewById(R.id.btn_hitung);
        btnReset = findViewById(R.id.btn_reset);
        btnHitung.setOnClickListener(v -> validasiCheck());
        btnReset.setOnClickListener(v -> reset());
    }

    private void reset() {
        edtLuasLahan.setText("");
        edtL.setText("");
        edtP.setText("");
        edtLuasLahan.requestFocus();
        hasilLayout.setVisibility(View.GONE);
    }

    private void validasiCheck() {
        Boolean validasi = true;
        if (edtP.getText().toString().isEmpty()) {
            edtP.setError("Harap diisi");
            edtP.requestFocus();
            validasi = false;
        } else {
            edtP.setError(null);
        }
        if (edtL.getText().toString().isEmpty()) {
            edtL.setError("Harap diisi");
            edtL.requestFocus();
            validasi = false;
        } else {
            edtL.setError(null);
        }
        if (edtLuasLahan.getText().toString().isEmpty()) {
            edtLuasLahan.setError("Harap diisi");
            edtLuasLahan.requestFocus();
            validasi = false;
        } else {
            edtLuasLahan.setError(null);
        }
        if (validasi) {
            hitung();
        }
    }

    @SuppressLint("SetTextI18n")
    private void hitung() {
        Double hasil = Perhitungan.populasi(
                Double.parseDouble(edtLuasLahan.getText().toString()),
                Double.parseDouble(edtP.getText().toString()),
                Double.parseDouble(edtL.getText().toString())
        );
        tvJumlahTanaman.setText("" + BigDecimal.valueOf(hasil).toPlainString()+" tanaman");

        hasilLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}