package example.pertanianjagung.assets;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.TextView;

import java.math.BigDecimal;

public class Perhitungan {

    public static double populasi(double lahan, double j1, double j2) {
        lahan = lahan * 10000;
        double jarak = (j1 / 100) * (j2 / 100);
        return lahan / jarak;
    }

    public static double jarak(double lahan, double bibit) {
        lahan = lahan * 100000000;
        return lahan / bibit;
    }

    public static Double produksi(Double lahan, Double j1, Double j2) {
        lahan *= 10000;
        double jarak = (j1 / 100) * (j2 / 100);
        double hasil = lahan / jarak;
        return ((hasil * 486) * 2.857142857142857e-4);
    }

    @SuppressLint("SetTextI18n")
    public static void pupuk(double lahan, double j1, double j2, double berat, TextView hasil1, TextView hasil2, TextView hasil3) {
        lahan *= 10000;
        double jarak = (j1 / 100) * (j2 / 100);
        double h1 = lahan / jarak;
        double h2 = (berat / h1) * 1000;
        double h3 = 0.3333333333333333 * h2;
        double h4 = h2 - h3;
        hasil1.setText(BigDecimal.valueOf(h2).toPlainString() + " gr/tanaman");
        hasil2.setText(BigDecimal.valueOf(h3).toPlainString() + " gr/tanaman");
        hasil3.setText(BigDecimal.valueOf(h4).toPlainString() + " gr/tanaman");
    }


//    public static double hitungPopulasi(int lahan, int j1, int j2) {
//
//    }

}
